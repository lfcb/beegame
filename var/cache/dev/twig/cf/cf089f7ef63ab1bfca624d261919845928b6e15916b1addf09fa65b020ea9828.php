<?php

/* @Game/Default/game.html.twig */
class __TwigTemplate_efe80ddee4ad4ab16f3aab96c5758c7183d5411acea62950f200d66eb73bcd81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle:Default:layout.html.twig", "@Game/Default/game.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Game/Default/game.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "

    <div class=\"middle-box text-center loginscreen animated fadeInDown\">
        <div>
            <br>
            <h3>Welcome to the Bee Game! </h3>
            <img id=\"bee\" class=\"img-bee\" src=\" ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/images/drone.jpg"), "html", null, true);
        echo " \" height=\"220\" width=\"220\" style=\"display: none\" />


            <form class=\"m-t\">
                <button id=\"playButton\" class=\"btn btn-primary full-width m-b\" >Start/Restart Game</button>
                <button id=\"hitButton\" class=\"btn btn-danger full-width m-b\" style=\"display: none\">Hit</button>
            </form>

            <p class=\"m-t\" id=\"descBee\" style=\"display: none\"> <b>Hello</b> I'm the <small id=\"kindBeeValue\"></small> Bee</p>
            <p class=\"m-t\" id=\"qtyBee\" style=\"display: none\"> <b>Alive: </b><small id=\"qtyBeeValue\"></small> </p>
            <p class=\"m-t\" id=\"lifespanBee\" style=\"display: none\"> <b>Lifespan:</b> <small id=\"lifespanBeeValue\"></small> </p>
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@Game/Default/game.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 11,  40 => 5,  34 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle:Default:layout.html.twig\" %}


{% block body %}


    <div class=\"middle-box text-center loginscreen animated fadeInDown\">
        <div>
            <br>
            <h3>Welcome to the Bee Game! </h3>
            <img id=\"bee\" class=\"img-bee\" src=\" {{ asset('bundles/app/images/drone.jpg') }} \" height=\"220\" width=\"220\" style=\"display: none\" />


            <form class=\"m-t\">
                <button id=\"playButton\" class=\"btn btn-primary full-width m-b\" >Start/Restart Game</button>
                <button id=\"hitButton\" class=\"btn btn-danger full-width m-b\" style=\"display: none\">Hit</button>
            </form>

            <p class=\"m-t\" id=\"descBee\" style=\"display: none\"> <b>Hello</b> I'm the <small id=\"kindBeeValue\"></small> Bee</p>
            <p class=\"m-t\" id=\"qtyBee\" style=\"display: none\"> <b>Alive: </b><small id=\"qtyBeeValue\"></small> </p>
            <p class=\"m-t\" id=\"lifespanBee\" style=\"display: none\"> <b>Lifespan:</b> <small id=\"lifespanBeeValue\"></small> </p>
        </div>
    </div>


{% endblock %}




", "@Game/Default/game.html.twig", "/private/var/www/beegame/src/GameBundle/Resources/views/Default/game.html.twig");
    }
}
