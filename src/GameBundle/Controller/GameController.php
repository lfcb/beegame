<?php

namespace GameBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GameBundle\Game;

use Symfony\Component\HttpFoundation\Session\Session;
class GameController extends Controller
{
    public $game;
      /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        unset ($_SESSION["droneBee"]);
        unset ($_SESSION["workerBee"]);
        unset ($_SESSION["queenBee"]);
        return $this->render("@Game/Default/game.html.twig");
    }

    /**
     * @Route("/hit-bee", name="hit-bee", options={"expose"=true})
     */
    public function hitBee(Request $request){
        $kind = $request->get("kind");

        $game = new Game(null, null, null);
        $game->hitAction($kind);
        //another function for HINT PASS NAME KIND BEE... //TODO:
        return new Response(json_encode([
            "success"=>true,
            "kindBee" => $game->beeForRound()['kindBee'],
            "qty" => $game->beeForRound()['qty'],
            "lifespan" => $game->beeForRound()['lifespan'],
            "nextRound" => $game->beeForRound()['nextRound'],
        ]),200);
    }

    /**
     * @Route("/life-cycle", name="life-cycle", options={"expose"=true})
     */
    public function LifeCycleGame(Request $request){
        $game = new Game(null, null, null);
        return new Response(json_encode([
            "success"=>true,
            "kindBee" => $game->beeForRound()['kindBee'],
            "qty" => $game->beeForRound()['qty'],
            "lifespan" => $game->beeForRound()['lifespan'],
        ]),200);
    }

}
