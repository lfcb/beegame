$(document).ready(function(){
    var play = $("#playButton");
    var bee = $("#bee");
    var hit = $("#hitButton");
    var desc = $("#descBee");
    var qty = $("#qtyBee");
    var lifespan = $("#lifespanBee");

    $(document).delegate("#playButton","click",function(evt){
        play.css("display" , "none");
        hit.css("display" , "block");
        evt.preventDefault();

        $.ajax({
            url: Routing.generate('life-cycle'),
            type: 'POST',
            dataType: 'json',
            data: {
                kind: $("#kindBeeValue").text(),
                qty: $("#qtyBeeValue").text(),
                lifespan: $("#lifespanBeeValue").text()
            }
        }).done(function(data){
            console.log(data);
            $("#kindBeeValue").text(data.kindBee);
            $("#qtyBeeValue").text(data.qty);
            $("#lifespanBeeValue").text(data.lifespan);

            bee.attr("src",'bundles/app/images/'+data.kindBee+'.jpg');
            bee.css("display" , "block");
            desc.css("display" , "block");
            qty.css("display" , "block");
            lifespan.css("display" , "block");

        }).fail(function(jqXHR, textStatus, errorThrown){

            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    });

    $(document).delegate("#hitButton","click",function(evt){

        evt.preventDefault();

        $.ajax({
            url: Routing.generate('hit-bee'),
            type: 'POST',
            dataType: 'json',
            data: {
                kind: $("#kindBeeValue").text()
            }
        }).done(function(data){
            console.log(data);
            $("#kindBeeValue").text(data.kindBee);
            $("#qtyBeeValue").text(data.qty);
            $("#lifespanBeeValue").text(data.lifespan);

            console.log("nextRound", data.nextRound);
            if(data.nextRound){
                nextRound();
            }else{
                bee.attr("src",'bundles/app/images/'+data.kindBee+'.jpg');
                bee.css("display" , "block");
                desc.css("display" , "block");
                qty.css("display" , "block");
                lifespan.css("display" , "block");
            }

        }).fail(function(jqXHR, textStatus, errorThrown){

            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    });

    function nextRound(){
        alert("Congratulations!, You win.");

        play.css("display" , "block");
        hit.css("display" , "none");
        bee.css("display" , "none");
        desc.css("display" , "none");
        qty.css("display" , "none");
        lifespan.css("display" , "none");
    }
});