<?php
/**
 * Created by PhpStorm.
 * User: luiscarbajal
 * Date: 01/02/2019
 * Time: 19:26
 */
namespace GameBundle;
class Game{
    private $kindBee;
    private $qtyBee;
    private $lifespanBee;
    private $nextRound = false;

    public function __construct(){
            $this->setNewBee();
    }

    private function setNewBee(){
        $bee = new Bee($this->giveNameOfBee());

        if(!$this->checkAllBeeDeaded($bee)){
            if($bee->getQty() === 0){
                return $this->setNewBee();
            }
        }

        $this->kindBee = $bee->getName();
        $this->qtyBee = $bee->getQty();
        $this->lifespanBee = $bee->getLifespan();
    }

    private function giveNameOfBee(){
        $randBee = rand(1, 6);

       if($randBee === 1 || $randBee === 6)
           return "Queen";

       if($randBee === 2 || $randBee === 5)
           return "Drone";

        if($randBee === 3 || $randBee === 4)
            return "Worker";
    }

    public function beeForRound(){
       return [
           'kindBee' => $this->kindBee,
           'qty' => $this->qtyBee,
           'lifespan' => $this->lifespanBee,
           'nextRound' => $this->nextRound,
       ];
    }

    public function hitAction($kindBee){
        $bee = new Bee($kindBee);
        $remainingLife = $bee->getLifespan() - $bee->getHitPoints();
        $remainingQty = $bee->getQty();

        /* BEE DEAD*/
        if($remainingLife === 0 || $remainingLife < 0){
            $remainingQty -= 1;
            $remainingLife = $bee->restartLifespan();
        }
        /* END */

        if($remainingQty == 0 || $remainingQty < 0){
            $remainingQty = 0;
        }

        $this->lifespanBee = $remainingLife;
        $this->qtyBee = $remainingQty;

        $bee->updateBee($remainingLife, $remainingQty);
        $this->setNewBee();
    }

    public function checkAllBeeDeaded(Bee $bee){
        $bool = false;
        $droneBeeQty = isset($_SESSION["droneBee"]) ? $_SESSION["droneBee"]['qty'] : null;
        $workerBeeQty = isset($_SESSION["workerBee"]) ? $_SESSION["workerBee"]['qty'] : null;
        $queenBeeQty = isset($_SESSION["queenBee"]) ? $_SESSION["queenBee"]['qty'] : null;

        if($droneBeeQty === 0 && $workerBeeQty === 0 && $queenBeeQty === 0){
            $bool = true;
            $this->nextRound = true;
            $bee->reviveBees();
        }

        if($queenBeeQty === 0){
            $bool = true;
            $this->nextRound = true;
            $bee->reviveBees();
        }

        return $bool;
    }
}