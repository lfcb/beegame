<?php
/**
 * Created by PhpStorm.
 * User: luiscarbajal
 * Date: 01/02/2019
 * Time: 19:26
 */
namespace GameBundle;
class Bee{
    public $name;
    public $lifespan;
    public $qty;
    public $hitPoints;

    public function __construct($name){
        if($name == "Queen"){
            $this->name = "Queen";
            $this->qty =isset($_SESSION["queenBee"]) ? $_SESSION["queenBee"]['qty'] : 1;
            $this->lifespan = isset($_SESSION["queenBee"]) && $_SESSION["queenBee"]['lifespan'] ? $_SESSION["queenBee"]['lifespan'] :100;
            $this->hitPoints = 8;
        }

        if($name == "Drone"){
            $this->name = "Drone";
            $this->qty =isset($_SESSION["droneBee"]) ? $_SESSION["droneBee"]['qty'] : 8;
            $this->lifespan = isset($_SESSION["droneBee"]) && $_SESSION["droneBee"]['lifespan'] ? $_SESSION["droneBee"]['lifespan'] :50;
            $this->hitPoints = 12;
        }

        if($name == "Worker"){
            $this->name = "Worker";
            $this->qty =isset($_SESSION["workerBee"]) ? $_SESSION["workerBee"]['qty'] : 5;
            $this->lifespan = isset($_SESSION["workerBee"]) && $_SESSION["workerBee"]['lifespan'] ? $_SESSION["workerBee"]['lifespan'] :75;
            $this->hitPoints = 10;
        }
    }

    public function updateBee($lifespan, $qty){
        if($this->name == "Queen"){
            $this->lifespan = $lifespan;
            $this->qty = $qty;
            $_SESSION["queenBee"] = [ 'lifespan' => $this->lifespan, 'qty'=> $this->qty ];
        }

        if($this->name == "Drone"){
            $this->lifespan = $lifespan;
            $this->qty = $qty;
            $_SESSION["droneBee"] = [ 'lifespan' => $this->lifespan, 'qty'=> $this->qty ];
        }

        if($this->name == "Worker"){
            $this->lifespan = $lifespan;
            $this->qty = $qty;
            $_SESSION["workerBee"] = [ 'lifespan' => $this->lifespan, 'qty'=> $this->qty ];
        }
    }

    public function restartLifespan(){
        $lifespan = 0;
        if($this->name == "Queen"){
            $lifespan = 100;
        }

        if($this->name == "Drone"){
            $lifespan = 50;
        }

        if($this->name == "Worker"){
            $lifespan = 75;
        }

        return $lifespan;
    }

    public function reviveBees(){
        $_SESSION["queenBee"]['qty'] = 1;
        $_SESSION["queenBee"]['lifespan'] = 100;

        $_SESSION["droneBee"]['qty'] = 8;
        $_SESSION["droneBee"]['lifespan'] =50;

        $_SESSION["workerBee"]['qty'] = 5;
        $_SESSION["workerBee"]['lifespan'] =75;
    }

    /**
     * @return int
     */
    public function getHitPoints()
    {
        return $this->hitPoints;
    }

    /**
     * @param int $hitPoints
     */
    public function setHitPoints($hitPoints)
    {
        $this->hitPoints = $hitPoints;
    }

    /**
     * @return mixed
     */
    public function getLifespan()
    {
        return $this->lifespan;
    }

    /**
     * @param mixed $lifespan
     */
    public function setLifespan($lifespan)
    {
        $this->lifespan = $lifespan;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

}